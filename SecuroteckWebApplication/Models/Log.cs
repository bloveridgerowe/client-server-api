﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace SecuroteckWebApplication.Models
{
    /// <summary>
    /// This class is used for entries in the Logs database, and the user.Log ICollection
    /// </summary>
    public class Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LogId { get; set;  }
        public string LogString { get; set; }
        public DateTime LogDateTime { get; set; }

        public Log()
        {

        }

        /// <summary>
        /// Constructor to instantiate a valid log
        /// </summary>
        /// <param name="LogString"></param>
        /// <param name="LogDateTime"></param>
        public Log(string LogString, DateTime LogDateTime)
        {
            this.LogString = LogString;
            this.LogDateTime = LogDateTime;
        }
    }

    /// <summary>
    /// This class is used for the LogArchives database
    /// </summary>
    public class LogArchive
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LogId { get; set; }
        public string LogApiKey { get; set; }
        public string LogUserName { get; set; }
        public string LogString { get; set; }
        public DateTime LogDateTime { get; set; }

        public LogArchive()
        {

        }

        /// <summary>
        /// Constructor to instantiate a valid archive log
        /// </summary>
        /// <param name="LogApiKey"></param>
        /// <param name="LogUserName"></param>
        /// <param name="LogString"></param>
        /// <param name="LogDateTime"></param>
        public LogArchive(string LogApiKey, string LogUserName, string LogString, DateTime LogDateTime)
        {
            this.LogApiKey = LogApiKey;
            this.LogUserName = LogUserName;
            this.LogString = LogString;
            this.LogDateTime = LogDateTime;
        }
    }
}