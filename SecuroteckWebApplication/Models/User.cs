﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using SecuroteckWebApplication.Controllers;

namespace SecuroteckWebApplication.Models
{
    /// <summary>
    /// Stores required data about each user
    /// </summary>
    public class User
    {
        #region Task2

        // TODO: Create a User Class for use with Entity Framework
        // Note that you can use the [key] attribute to set your ApiKey Guid as the primary key 

        #endregion

        [Key]
        public string ApiKey { get; set; }
        public string UserName { get;  set; }
        public virtual ICollection<Log> Logs { get; private set; }

        public User()
        {

        }
    }

    /// <summary>
    /// Contains methods which conduct database access and manipulation
    /// </summary>
    public class UserDatabaseAccess
    {
        /// <summary>
        /// Adds a log to the logs database
        /// </summary>
        /// <param name="key"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool AddLog(string key, string message)
        {
            using (var context = new UserContext())
            {
                if (String.IsNullOrEmpty(key) || String.IsNullOrEmpty(message)) return false;
                var user = context.Users.Find(key);
                if (user != null)
                {
                    user.Logs.Add(new Log(message, DateTime.Now));
                    context.SaveChanges();
                }
                return true;
            }
        }

        /// <summary>
        /// Creates a new user in the user database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public string CreateUser(string username)
        {
            using (var context = new UserContext())
            {
                var user = new User()
                {
                    ApiKey = Guid.NewGuid().ToString(),
                    UserName = username
                };

                context.Users.Add(user);
                context.SaveChanges();
                return user.ApiKey;
            }
        }

        /// <summary>
        /// Checks whether a user is present in the database using their API key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool CheckByApiKey(string key)
        {
            using (var context = new UserContext())
            {
                var user = context.Users.Find(key);
                return user != null;
            }
        }

        /// <summary>
        /// Checks whether a user is present in the database using their user name
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool CheckByUserName(string username)
        {
            using (var context = new UserContext())
            {
                var user = context.Users.SingleOrDefault(us => us.UserName == username);
                if (user != null) return user.UserName == username;
                return false;
            }
        }

        /// <summary>
        /// Cross validate username and API key to ensure they match the database
        /// </summary>
        /// <param name="key"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool CheckByApiKeyAndUsername(string key, string username)
        {
            using (var context = new UserContext())
            {
                var user = context.Users.Find(key);
                if (user == null) return false;
                return user.UserName == username;
            }
        }

        /// <summary>
        /// Finds a user from an API key (if present), and returns it
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public User GetUserByApiKey(string key)
        {
            using (var context = new UserContext())
            {
                var user = context.Users.Find(key);
                return user;
            }
        }

        /// <summary>
        /// Delete user with the passed API key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool DeleteUserByApiKey(string key)
        {
            using (var context = new UserContext())
            {
                var user = context.Users.Find(key);
                if (user == null) return false;

                foreach (Log l in user.Logs.ToList())
                {
                    LogArchive la = new LogArchive(user.ApiKey, user.UserName, l.LogString, l.LogDateTime);
                    context.LogArchive.Add(la);
                    context.Logs.Remove(l);
                }

                user.Logs.Clear();
                context.Users.Remove(user);
                context.SaveChanges();
                return true;
            }
        }
    }
}