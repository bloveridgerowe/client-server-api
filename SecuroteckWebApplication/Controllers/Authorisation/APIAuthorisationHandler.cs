﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using SecuroteckWebApplication.Models;

namespace SecuroteckWebApplication.Controllers
{
    public class APIAuthorisationHandler : DelegatingHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync (HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<string> headers = request.Headers.GetValues("ApiKey");
                var apiKey = headers.FirstOrDefault();
                var dba = new UserDatabaseAccess();

                if (dba.CheckByApiKey(apiKey) == true)
                {
                    var usr = dba.GetUserByApiKey(apiKey);

                    var claims = new Claim[1];
                    claims[0] = new Claim(ClaimTypes.Name, usr.UserName);

                    var claim = new ClaimsIdentity(claims, apiKey);
                    var principle = new ClaimsPrincipal(claim);

                    Thread.CurrentPrincipal = principle;
                }
            }
            catch
            {

            }
            return base.SendAsync(request, cancellationToken);
        }
    }
}