﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using SecuroteckWebApplication.Models;

namespace SecuroteckWebApplication.Controllers
{
    public class ProtectedController : ApiController
    {
        /// <summary>
        /// Get API key from header, use it to find the user that made the request
        /// Add request to the users log
        /// Return Hello "username"
        /// </summary>
        /// <returns></returns>
        [CustomAuthorise]
        [ActionName("Hello")]
        public HttpResponseMessage Get()
        {
            IEnumerable<string> headers = Request.Headers.GetValues("ApiKey");
            var apiKey = headers.FirstOrDefault();
            var dba = new UserDatabaseAccess();
            var usr = dba.GetUserByApiKey(apiKey);
            dba.AddLog(apiKey, "User accessed api/protected/hello");

            return Request.CreateResponse(HttpStatusCode.OK, "Hello " + usr.UserName);
        }

        /// <summary>
        /// Converts a raw byte array to a hex string
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public string ConvertByteToHex(byte[] bytes)
        {
            //return BitConverter.ToString(bytes).Replace("-", "");
            return BitConverter.ToString(bytes);
        }

        /// <summary>
        /// Signs a given string message
        /// Add request to the users log
        /// Returns the hexadecimal output
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [CustomAuthorise]
        [Route("api/protected/sign")]
        public HttpResponseMessage GetSign(string message)
        {
            if (String.IsNullOrEmpty(message))
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");

            IEnumerable<string> headers = Request.Headers.GetValues("ApiKey");
            var apiKey = headers.FirstOrDefault();
            var dba = new UserDatabaseAccess();
            var usr = dba.GetUserByApiKey(apiKey);
            dba.AddLog(apiKey, "User accessed api/protected/sign");

            var message_utf8 = System.Text.Encoding.UTF8.GetBytes(message);
            var result = WebApiConfig.RsaCrypto.SignData(message_utf8, "SHA1");
            return Request.CreateResponse(HttpStatusCode.OK, ConvertByteToHex(result));
        }

        /// <summary>
        /// Returns the public key
        /// </summary>
        /// <returns></returns>
        [CustomAuthorise]
        [Route("api/protected/getpublickey")]
        public HttpResponseMessage GetPublicKey()
        {
            IEnumerable<string> headers = Request.Headers.GetValues("ApiKey");
            var apiKey = headers.FirstOrDefault();
            var dba = new UserDatabaseAccess();
            var usr = dba.GetUserByApiKey(apiKey);
            dba.AddLog(apiKey, "User accessed api/protected/getpublickey");

            return Request.CreateResponse(HttpStatusCode.OK, WebApiConfig.RsaCrypto.ToXmlString(true));
        }

        /// <summary>
        /// Converts a string to a SHA1 hash
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [CustomAuthorise]
        [Route("api/protected/sha1")]
        public HttpResponseMessage GetSHA1([FromUri] string message = "")
        {
            if (String.IsNullOrEmpty(message))
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");

            IEnumerable<string> headers = Request.Headers.GetValues("ApiKey");
            var apiKey = headers.FirstOrDefault();
            var dba = new UserDatabaseAccess();
            var usr = dba.GetUserByApiKey(apiKey);
            dba.AddLog(apiKey, "User accessed api/protected/sha1");

            var provider = new SHA1CryptoServiceProvider();
            var message_utf8 = System.Text.Encoding.UTF8.GetBytes(message);
            var message_sha1 = provider.ComputeHash(message_utf8);
            var message_sha1_hex = ConvertByteToHex(message_sha1);
            message_sha1_hex = message_sha1_hex.Replace("-", "");
            return Request.CreateResponse(HttpStatusCode.OK, message_sha1_hex);
        }

        /// <summary>
        /// Converts a string to a SHA256 hash
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [CustomAuthorise]
        [Route("api/protected/sha256")]
        public HttpResponseMessage GetSHA256([FromUri] string message = "")
        {
            if (String.IsNullOrEmpty(message))
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");

            IEnumerable<string> headers = Request.Headers.GetValues("ApiKey");
            var apiKey = headers.FirstOrDefault();
            var dba = new UserDatabaseAccess();
            var usr = dba.GetUserByApiKey(apiKey);
            dba.AddLog(apiKey, "User accessed api/protected/sha256");
            
            var provider = new SHA256CryptoServiceProvider();
            var message_utf8 = System.Text.Encoding.UTF8.GetBytes(message);
            var message_sha256 = provider.ComputeHash(message_utf8);
            var message_sha256_hex = ConvertByteToHex(message_sha256);
            message_sha256_hex = message_sha256_hex.Replace("-", "");
            return Request.CreateResponse(HttpStatusCode.OK, message_sha256_hex);
        }
    }
}
