﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SecuroteckWebApplication.Controllers
{
    public class TalkBackController : ApiController
    {
        /// <summary>
        /// Returns Hello World
        /// </summary>
        /// <returns></returns>
        [ActionName("Hello")]
        public HttpResponseMessage Get()
        {
            #region TASK1
            // TODO: add api/talkback/hello response
            #endregion
            return Request.CreateResponse(HttpStatusCode.OK, "Hello World");
        }

        /// <summary>
        /// Attempt to convert string array to integer array
        /// If parsing fails, a character is present, return a bad HTTP request
        /// Sort the newly instantiated integer array, and return it
        /// </summary>
        /// <param name="integers"></param>
        /// <returns></returns>
        [ActionName("Sort")]
        public HttpResponseMessage Get([FromUri]string[] integers)
        {
            #region TASK1
            // TODO: 
            // sort the integers into ascending order
            // send the integers back as the api/talkback/sort response
            #endregion

            if(integers.Length == 0) Request.CreateResponse(HttpStatusCode.OK, new int[0]);

            int[] parsedIntegers = new int[integers.Length];

            try
            {
                for (int i = 0; i < integers.Length; i++) parsedIntegers[i] = int.Parse(integers[i]);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad request");
            }

            Array.Sort(parsedIntegers);
            return Request.CreateResponse(HttpStatusCode.OK, parsedIntegers);
        }

    }
}
