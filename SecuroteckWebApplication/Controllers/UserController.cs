﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SecuroteckWebApplication.Models;

namespace SecuroteckWebApplication.Controllers
{
    public class UserController : ApiController
    {
        /// <summary>
        /// Checks to see if a user exists in the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [ActionName("New")]
        public HttpResponseMessage Get([FromUri]string username = "")
        {
            var dba = new UserDatabaseAccess();
            if (!String.IsNullOrEmpty(username))
            {
                if (dba.CheckByUserName(username) == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        "True - User Does Exist! Did you mean to do a POST to create a new user?");
                }
                return Request.CreateResponse(HttpStatusCode.OK,
                    "False - User Does Not Exist! Did you mean to do a POST to create a new user?");
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest,
                "False - User Does Not Exist! Did you mean to do a POST to create a new user?");
        }

        /// <summary>
        /// Adds a new user to the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [ActionName("New")]
        public HttpResponseMessage Post([FromBody]string username)
        {
            var dba = new UserDatabaseAccess();
            if (string.IsNullOrEmpty(username))
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    "Oops. Make sure your body contains a string with your username" +
                       "and your Content-Type is Content-Type:application/json");

            return Request.CreateResponse(HttpStatusCode.OK,dba.CreateUser(username));
        }

        /// <summary>
        /// Removes a user from the database, if they exist
        /// </summary>
        /// <param name="username"></param>
        /// <param name="header"></param>
        /// <returns></returns>
        [CustomAuthorise]
        [ActionName("RemoveUser")]
        public HttpResponseMessage Delete([FromUri] string username, [FromBody] string header)
        {
            IEnumerable<string> headers = Request.Headers.GetValues("ApiKey");
            var apiKey = headers.FirstOrDefault();
            var dba = new UserDatabaseAccess();
            if (dba.CheckByApiKey(apiKey) == true)
            {
                var usr = dba.GetUserByApiKey(apiKey);

                if (usr.UserName == username)
                {
                    dba.DeleteUserByApiKey(apiKey);
                    return Request.CreateResponse(HttpStatusCode.OK, true);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, false);
        }
    }
}