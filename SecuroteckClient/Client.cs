﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace SecuroteckClient
{
    class Client
    {
        static HttpClient client = new HttpClient();
        private static string userName = "";
        private static string apiKey = "";
        private static string publicKey = "";
        private static bool firstTime = true;

        /// <summary>
        /// Set up the HTTP client, then run MainAsync to handle user input
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            client.BaseAddress = new Uri("http://localhost:24702/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            MainAsync().Wait();
        }

        /// <summary>
        /// Gets the users request, and calls the appropriate async task to handle it
        /// </summary>
        /// <returns></returns>
        static async Task MainAsync()
        {
            FindCurrentUser();
            ShowCurrentUser();

            while (true)
            {
                Console.Write(firstTime ? "What would you like to do? " : "What would you like to do next? ");
                var response = Console.ReadLine();

                if (String.IsNullOrWhiteSpace(response))
                {
                    Console.WriteLine("Please enter a valid command\r\n");
                    continue;
                }

                var responseArgs = response.Split(new char[] {' '}, 3);
                responseArgs[0] = responseArgs[0].ToLower();
                if (responseArgs.Length > 1) responseArgs[1] = responseArgs[1].ToLower();

                try
                {
                    switch (responseArgs[0])
                    {
                        case "talkback":
                            if (responseArgs[1] == "hello")
                            {
                                string serverResponse = "";
                                Console.WriteLine("...please wait...");
                                serverResponse = await GetTalkBackHello();
                            }
                            else if (responseArgs[1] == "sort")
                            {
                                Console.WriteLine("...please wait...");
                                var sorted = await GetTalkBackSort(responseArgs[2]);
                            }
                            else
                            {
                                Console.WriteLine("Please enter a valid command\r\n");
                            }
                            break;
                        case "user":
                            if (responseArgs[1] == "get")
                            {
                                string serverResponse = "";
                                Console.WriteLine("...please wait...");
                                serverResponse = await GetUser(responseArgs[2]);
                            }
                            else if (responseArgs[1] == "post")
                            {
                                string serverResponse = "";
                                Console.WriteLine("...please wait...");
                                serverResponse = await PostUser(responseArgs[2]);
                                userName = responseArgs[2];
                                apiKey = serverResponse;
                                if (serverResponse != "failed")
                                {
                                    Console.WriteLine("Got API Key");
                                    SetCurrentUser();
                                    ShowCurrentUser();
                                }
                            }
                            else if (responseArgs[1] == "set")
                            {
                                var userDetails = responseArgs[2].Split(' ');
                                userName = userDetails[0];
                                apiKey = userDetails[1];
                                SetCurrentUser();
                                ShowCurrentUser();
                            }
                            else if (responseArgs[1] == "delete")
                            {
                                if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(apiKey))
                                {
                                    Console.WriteLine("You need to do a User Post or User Set first\r\n");
                                    break;
                                }
                                string serverResponse = "";
                                Console.WriteLine("...please wait...");
                                serverResponse = await DeleteUser();
                                if (serverResponse == "true")
                                {
                                    userName = "";
                                    apiKey = "";
                                    SetCurrentUser();
                                    ShowCurrentUser();
                                }
                            }
                            else
                            {
                                Console.WriteLine("Please enter a valid command\r\n");
                            }
                            break;
                        case "protected":
                            if (responseArgs[1] == "hello")
                            {
                                string serverResponse = "";
                                Console.WriteLine("...please wait...");
                                serverResponse = await GetProtectedHello();
                            }
                            else if (responseArgs[1] == "sha1")
                            {
                                string serverResponse = "";
                                Console.WriteLine("...please wait...");
                                serverResponse = await GetProtectedSha1(responseArgs[2]);
                            }
                            else if (responseArgs[1] == "sha256")
                            {
                                string serverResponse = "";
                                Console.WriteLine("...please wait...");
                                serverResponse = await GetProtectedSha256(responseArgs[2]);
                            }
                            else if (responseArgs[1] == "get")
                            {
                                if (responseArgs.Length > 2)
                                {
                                    if (responseArgs[2].ToLower() == "publickey")
                                    {
                                        string serverResponse = "";
                                        Console.WriteLine("...please wait...");
                                        serverResponse = await GetProtectedPublicKey();
                                        if (serverResponse != "failed") publicKey = serverResponse;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Please enter a valid command\r\n");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Please enter a valid command\r\n");
                                }
                            }
                            else if (responseArgs[1] == "sign")
                            {
                                string serverResponse = "";
                                if (String.IsNullOrEmpty(publicKey))
                                {
                                    Console.WriteLine("Response: client doesn’t yet have the public key\r\n");
                                    break;
                                }
                                Console.WriteLine("...please wait...");
                                serverResponse = await GetProtectedSign(responseArgs[2]);
                            }
                            else
                            {
                                Console.WriteLine("Please enter a valid command\r\n");
                            }
                            break;
                        case "help":
                            DisplayHelp();
                            break;
                        case "show":
                            ShowCurrentUser();
                            break;
                        case "exit":
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Please enter a valid command\r\n");
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Please enter a valid command\r\n");
                }
                finally
                {
                    firstTime = false;
                }
            }
        }

        /// <summary>
        /// Converts a string to a byte array
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] StringToByteArray(string hex)
        {
            hex = hex.Replace("-", "");

            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        /// <summary>
        /// If user details text file exists, open it and load the saved data
        /// </summary>
        static void FindCurrentUser()
        {
            try
            {
                using (StreamReader reader = new StreamReader("user details.txt"))
                {
                    userName = reader.ReadLine()?.Replace("\"", "");
                    apiKey = reader.ReadLine()?.Replace("\"", "").Replace("\\", "");
                }

                if (client.DefaultRequestHeaders.Contains("ApiKey")) client.DefaultRequestHeaders.Remove("ApiKey");
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
            }
            catch
            { }
        }

        /// <summary>
        /// Will set the current active user, and save the details to user details text file
        /// </summary>
        static void SetCurrentUser()
        {
            using (StreamWriter writer = new StreamWriter("user details.txt"))
            {
                writer.WriteLine(userName.Replace("\"", ""));
                writer.WriteLine(apiKey.Replace("\"", "").Replace("\\", ""));
            }

            if (client.DefaultRequestHeaders.Contains("ApiKey")) client.DefaultRequestHeaders.Remove("ApiKey");
            client.DefaultRequestHeaders.Add("ApiKey", apiKey);
        }

        /// <summary>
        /// Prints details of the current active user to the console
        /// </summary>
        static void ShowCurrentUser()
        {
            if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(apiKey))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\r\n==============================================");
                Console.WriteLine("Current User Details:");
                Console.WriteLine("No user stored");
                Console.WriteLine("==============================================\r\n");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\r\n==============================================");
                Console.WriteLine("Current User Details:");
                Console.WriteLine("Username: " + userName);
                Console.WriteLine("API Key: " + apiKey);
                Console.WriteLine("==============================================\r\n");
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Prints the deduced arguments
        /// </summary>
        /// <param name="args"></param>
        static void PrintArgs(string[] args)
        {
            Console.WriteLine("Controller: " + args[0]);
            Console.WriteLine("Action: " + args[1]);
            if (args.Length > 2) Console.WriteLine("Arguments: " + args[2]);
        }

        /// <summary>
        /// Prints available commands to the console
        /// </summary>
        static void DisplayHelp()
        {
            Console.WriteLine("\r\n--TalkBack");
            Console.WriteLine("------Hello");
            Console.WriteLine("------Sort    [1,2,3,4,5]");
            Console.WriteLine("\r\n--User");
            Console.WriteLine("------Get     <name>");
            Console.WriteLine("------Post    <name>");
            Console.WriteLine("------Set     <name> <apikey>");
            Console.WriteLine("------Delete");
            Console.WriteLine("\r\n--Protected");
            Console.WriteLine("------Hello");
            Console.WriteLine("------SHA1    <message>");
            Console.WriteLine("------SHA256  <message>");
            Console.WriteLine("------Sign    <message>");
            Console.WriteLine("------Get     PublicKey");
            Console.WriteLine("\r\n--Show User");
            Console.WriteLine("\r\n--Exit\r\n");
        }

        /// <summary>
        /// Converts a string to an integer array, by filtering the integers and parsing them
        /// </summary>
        /// <param name="stringArray"></param>
        /// <returns></returns>
        static int[] StrToIntArray(string stringArray)
        {
            return Regex.Matches(stringArray, "(-?[0-9]+)").OfType<Match>().Select(m => int.Parse(m.Value)).ToArray();
        }

        /// <summary>
        /// Retrieve the public key from the server
        /// </summary>
        /// <returns></returns>
        static async Task<string> GetProtectedPublicKey()
        {
            HttpResponseMessage response = await client.GetAsync("api/protected/getpublickey");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response: got public key\r\n");
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + ", could not get public key\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Make a protected hello request
        /// </summary>
        /// <returns></returns>
        static async Task<string> GetProtectedHello()
        {
            HttpResponseMessage response = await client.GetAsync("api/protected/hello");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response: " + content + "\r\n");
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Ask the server to sign a message string
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        static async Task<string> GetProtectedSign(string message)
        {
            HttpResponseMessage response = await client.GetAsync("api/protected/sign?message=" + message);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                RSACryptoServiceProvider clientRSAProvider = new RSACryptoServiceProvider();
                clientRSAProvider.FromXmlString(TrimJsonString(publicKey));

                Console.Write("Response: ");
                Console.WriteLine(clientRSAProvider.VerifyData(
                    Encoding.UTF8.GetBytes(message), new SHA1CryptoServiceProvider(), StringToByteArray(TrimJsonString(content)))
                    ? "message successfully signed\r\n" : "message was not successfully signed\r\n");
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Send a request to the server to convert a string to a SHA1 hash
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        static async Task<string> GetProtectedSha1(string message)
        {
            HttpResponseMessage response = await client.GetAsync("api/protected/sha1?message=" + message);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response: " + content + "\r\n");
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Send a request to the server to convert a string to a SHA256 hash
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        static async Task<string> GetProtectedSha256(string message)
        {
            HttpResponseMessage response = await client.GetAsync("api/protected/sha256?message=" + message);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response: " + content + "\r\n");
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Checks if a specified user is present on the server database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        static async Task<String> GetUser(string user)
        {
            HttpResponseMessage response = await client.GetAsync("api/user/new?username=" + user);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response: " + content + "\r\n");
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Sends a request to the server to delete the current active user
        /// </summary>
        /// <returns></returns>
        static async Task<string> DeleteUser()
        {
            HttpResponseMessage response = await client.DeleteAsync("api/user/removeuser?username=" + userName);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response: " + content);
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Sends a request to the server to create a new user with the specified name
        /// If a new user has been successfully added, their API key is returned
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        static async Task<String> PostUser(string user)
        {
            var userKVP = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("", user)
            });

            HttpResponseMessage response = await client.PostAsync("api/user/new", userKVP);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                content = content.Replace("\"", "").Replace("\\", "");
                Console.WriteLine("Response: " + content);
                return content;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Sends a talkback hello request to the server
        /// </summary>
        /// <returns></returns>
        static async Task<String> GetTalkBackHello()
        {
            HttpResponseMessage response = await client.GetAsync("api/talkback/hello");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response: " + content + "\r\n");
                return content;
            }

            else
            {
                Console.WriteLine("Response: request failed, HTTP status code " + response.StatusCode + "\r\n");
                return "failed";
            }
        }

        /// <summary>
        /// Composes URI from integer array string, and sends a sort request to the server
        /// </summary>
        /// <param name="integers"></param>
        /// <returns></returns>
        static async Task<int[]> GetTalkBackSort(string integers)
        {
            String uri = "api/talkback/sort";
            int[] sortedInts = new int[0];
            string[] strArray = integers.Trim('[').Trim(']').Split(',');

            HttpResponseMessage response;

            if (integers == "[]" || String.IsNullOrEmpty(integers))
            {
                response = await client.GetAsync(uri);
            }
            else
            {
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (i == 0) uri += "?integers=" + strArray[i];
                    else uri += "&integers=" + strArray[i];
                }
                response = await client.GetAsync(uri);
            }
            
            if (response.IsSuccessStatusCode)
            {
                var sorted = await response.Content.ReadAsStringAsync();
                sortedInts = StrToIntArray(sorted);
                Console.WriteLine("Response: " + sorted.Replace("\"", "") + "\r\n");
                return sortedInts;
            }
            else
            {
                Console.WriteLine("Response: request failed, HTTP status code " + response.StatusCode + "\r\n");
                return sortedInts;
            }
        }

        /// <summary>
        /// Trims the leading and trailing " and / from JSON strings
        /// </summary>
        /// <param name="toTrim"></param>
        /// <returns></returns>
        static string TrimJsonString(string toTrim)
        {
            string trimmed = toTrim.Remove(0, 1);
            trimmed = trimmed.Substring(0, trimmed.Length - 1);
            return trimmed;
        }
    }
}
